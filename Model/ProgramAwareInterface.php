<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Program\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface ProgramAwareInterface
{
    /**
     * @return ProgramInterface|null
     */
    public function getProgram(): ?ProgramInterface;

    /**
     * @param ProgramInterface|null $program
     *
     * @return ProgramAwareInterface|self
     */
    public function setProgram(?ProgramInterface $program): self;
}
