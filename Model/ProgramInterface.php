<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Program\Model;

use DateTimeInterface;
use Inisiatif\Component\Contract\Resource\Model\ResourceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface ProgramInterface extends ResourceInterface
{
    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @param string|null $value
     *
     * @return ProgramInterface|self
     */
    public function setName(?string $value): self;

    /**
     * @return DateTimeInterface|null
     */
    public function getEndDate(): ?DateTimeInterface;

    /**
     * @param DateTimeInterface|null $value
     *
     * @return ProgramInterface|self
     */
    public function setEndDate(?DateTimeInterface $value): self;
}
