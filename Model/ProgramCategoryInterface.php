<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Program\Model;

use Inisiatif\Component\Contract\Resource\Model\ResourceInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface ProgramCategoryInterface extends ResourceInterface
{
    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @param string|null $value
     *
     * @return ProgramCategoryInterface|self
     */
    public function setName(?string $value): self;
}
