<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Program\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface PlainCategoryInterface
{
    /**
     * @return string|null
     */
    public function getCategory(): ?string;

    /**
     * @param string|null $value
     *
     * @return PlainCategoryInterface|self
     */
    public function setCategory(?string $value): self;
}
