<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Program\Model;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface ProgramCategoryAwareInterface
{
    /**
     * @return ProgramCategoryInterface|null
     */
    public function getCategory(): ?ProgramCategoryInterface;

    /**
     * @param ProgramCategoryInterface|null $category
     *
     * @return ProgramCategoryAwareInterface|self
     */
    public function setCategory(?ProgramCategoryInterface $category): self;
}
