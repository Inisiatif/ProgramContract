<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Program\Repository;

use Inisiatif\Component\Contract\Program\Model\ProgramInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface ProgramRepositoryInterface
{
    /**
     * @param string $id
     *
     * @return ProgramInterface|null
     */
    public function findById(string $id): ?ProgramInterface;

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function fetch(array $params);
}
