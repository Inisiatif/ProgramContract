<?php declare(strict_types=1);

namespace Inisiatif\Component\Contract\Program\Service;

use Inisiatif\Component\Contract\Program\Model\ProgramInterface;

/**
 * @author Nuradiyana <me@nooradiana.com>
 */
interface ProgramServiceInterface
{
    /**
     * @param ProgramInterface $program
     *
     * @return ProgramInterface
     */
    public function save(ProgramInterface $program): ProgramInterface;
}
